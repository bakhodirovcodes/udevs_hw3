import React, { useState, useEffect, useMemo, useCallback } from "react"
import { TodoForm } from "./TodoForm"
import { v4 as uuidv4 } from "uuid"
import { Todo } from "./Todo"
import { EditTodoForm } from "./EditTodoForm"
uuidv4()

export const TodoWrapperLocalStorage = () => {
  const [todos, setTodos] = useState([])

  useEffect(() => {
    const savedTodos = JSON.parse(localStorage.getItem("todos")) || []
    setTodos(savedTodos)
  }, [])

  const memoizedTodos = useMemo(() => {
    localStorage.setItem("todos", JSON.stringify(todos))
    return todos
  }, [todos])

  const addTodo = useCallback(
    (todo) => {
      const newTodos = [
        ...memoizedTodos,
        { id: uuidv4(), task: todo, completed: false, isEditing: false },
      ]
      setTodos(newTodos)
    },
    [memoizedTodos]
  )

  const toggleComplete = useCallback(
    (id) => {
      const newTodos = memoizedTodos.map((todo) =>
        todo.id === id ? { ...todo, completed: !todo.completed } : todo
      )
      setTodos(newTodos)
    },
    [memoizedTodos]
  )

  const deleteTodo = useCallback(
    (id) => {
      const newTodos = memoizedTodos.filter((todo) => todo.id !== id)
      setTodos(newTodos)
    },
    [memoizedTodos]
  )

  const editTodo = useCallback(
    (id) => {
      setTodos(
        memoizedTodos.map((todo) =>
          todo.id === id ? { ...todo, isEditing: !todo.isEditing } : todo
        )
      )
    },
    [memoizedTodos]
  )

  const editTask = useCallback(
    (task, id) => {
      const newTodos = memoizedTodos.map((todo) =>
        todo.id === id ? { ...todo, task, isEditing: !todo.isEditing } : todo
      )
      setTodos(newTodos)
    },
    [memoizedTodos]
  )

  return (
    <div className="TodoWrapper">
      <h1>Get Things Done!</h1>
      <TodoForm addTodo={addTodo} />
      {todos.map((todo, index) =>
        todo.isEditing ? (
          <EditTodoForm editTodo={editTask} task={todo} />
        ) : (
          <Todo
            task={todo}
            key={index}
            toggleComplete={toggleComplete}
            deleteTodo={deleteTodo}
            editTodo={editTodo}
          />
        )
      )}
    </div>
  )
}
